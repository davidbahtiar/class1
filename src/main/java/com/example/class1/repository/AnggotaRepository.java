package com.example.class1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.AnggotaModel;

public interface AnggotaRepository extends JpaRepository<AnggotaModel, String> {

	@Query("SELECT F FROM AnggotaModel F WHERE kodeAnggota = ?1") // ?1 untuk mencari semua data
	AnggotaModel searchkodeAnggota(String kodeAnggota);
	
	@Query("SELECT F FROM AnggotaModel F WHERE namaAnggota LIKE %?1% ") //'%?1%' untuk mencari suatu data yang berawal dari ?1 / bisa jadi mencari data yang berawal huruf 'ad'
	List<AnggotaModel> searchnamaAnggota(String namaAnggota);

	@Query("SELECT F FROM AnggotaModel F WHERE kodeAnggota LIKE %?1%")
	List<AnggotaModel> cariAnggotaRepository(String kodeAnggota);
}
