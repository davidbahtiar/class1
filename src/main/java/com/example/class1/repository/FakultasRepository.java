package com.example.class1.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.class1.model.FakultasModel;

public interface FakultasRepository extends JpaRepository<FakultasModel, String> {
	
	@Query("SELECT F FROM FakultasModel F WHERE F.kodeFakultas = ?1") // ?1 untuk mencari semua data
	FakultasModel searchKodeFakultas(String kodeFakultas);
	
	@Query("SELECT F FROM FakultasModel F WHERE F.namaFakultas LIKE %?1% ") //'%?1%' untuk mencari suatu data yang berawal dari ?1 / bisa jadi mencari data yang berawal huruf 'ad'
	List<FakultasModel> searchNamaFakultas(String namaFakultas);

	@Query("SELECT F FROM FakultasModel F WHERE F.kodeFakultas LIKE %?1%")
	List<FakultasModel> cariFakultasDonkRepository(String kodeFakultas);
}
