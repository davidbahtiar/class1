package com.example.class1.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.class1.model.AnggotaModel;
import com.example.class1.service.AnggotaService;

import java.util.List;
import java.util.ArrayList;

@Controller
@RequestMapping ("/anggota")
public class AnggotaCotroller {

	@Autowired
	private AnggotaService anggotaService;
	
	@RequestMapping("/home")
	public String anggota()
	{
		return "/anggota/home";
	}
	
	@RequestMapping("/add")
	public String doAdd() {
		return "/anggota/add";
	}
	@RequestMapping("/create")
	public String doCreate(HttpServletRequest request) {
		String kodeAnggota = request.getParameter("kodeAnggota");
		String namaAnggota = request.getParameter("namaAnggota");
		Integer idAnggota = Integer.parseInt(request.getParameter("idAnggota"));
		Integer usia = Integer.parseInt(request.getParameter("usia"));
		
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel.setKodeAnggota(kodeAnggota);
		anggotaModel.setNamaAnggota(namaAnggota);
		anggotaModel.setIdAnggota(idAnggota);
		anggotaModel.setUsia(usia);
		
		this.anggotaService.save(anggotaModel);
		
		return "/anggota/home";
	}
	
	@RequestMapping("/data")
	public String doList(Model model)
	{
		List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
		anggotaModelList = this.anggotaService.read();
		model.addAttribute("anggotaModelList", anggotaModelList);
		
		return "/anggota/list";
	}
	
	@RequestMapping("/detail")
	public String doDetail(HttpServletRequest request, Model model)
	{
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);
		
		return "/anggota/detail";
	}
	
	@RequestMapping("/ubah")
	public String doUbah(HttpServletRequest request, Model model)
	{
		String kodeAnggota = request.getParameter("kodeAnggota");
		AnggotaModel anggotaModel = new AnggotaModel();
		anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
		model.addAttribute("anggotaModel", anggotaModel);
		
		return "/anggota/edit";
	}
	
	@RequestMapping("/update")
	public String doUpdate(HttpServletRequest request, Model model)
	{
	String kodeAnggota = request.getParameter("kodeAnggota");
	String namaAnggota = request.getParameter("namaAnggota");
	Integer idAnggota = Integer.parseInt(request.getParameter("idAnggota"));
	Integer usiaAnggota = Integer.parseInt(request.getParameter("usiaAnggota"));
	
	AnggotaModel anggotaModel = new AnggotaModel();
	anggotaModel.setIdAnggota(idAnggota);
	anggotaModel.setKodeAnggota(kodeAnggota);
	anggotaModel.setNamaAnggota(namaAnggota);
	anggotaModel.setUsia(usiaAnggota);
	this.anggotaService.update(anggotaModel);
	
	return "/anggota/home";
	
	}
	
	@RequestMapping("/hapus")
	public String doHapus(HttpServletRequest request, Model model){
	String kodeAnggota = request.getParameter("kodeAngota");
	AnggotaModel anggotaModel = new AnggotaModel();
	anggotaModel = this.anggotaService.searchKodeAnggota(kodeAnggota);
	this.anggotaService.delete(anggotaModel);
	
	return "/anggota/home";
	
	}
	
	
	@RequestMapping("/search")
	public String doSearchNama(HttpServletRequest request, Model model){
	String namaAnggota = request.getParameter("namaAnggota");
	
	List<AnggotaModel> anggotaModelList = new ArrayList<AnggotaModel>();
	anggotaModelList = this.anggotaService.searchNamaAnggota(namaAnggota);
	model.addAttribute("anggotaModelList", anggotaModelList);
	
	return "/anggota/search";
	
	}
}  // anggota controller