package com.example.class1.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.class1.model.AnggotaModel;
import com.example.class1.repository.AnggotaRepository;

@Service
@Transactional
public class AnggotaService {

	@Autowired
	private AnggotaRepository anggotaRepository;

	public void save(AnggotaModel anggotaModel) {
		anggotaRepository.save(anggotaModel);
	}

	public List<AnggotaModel> read() {
		return this.anggotaRepository.findAll();
	}

	public AnggotaModel searchKodeAnggota(String kodeAnggota) {

		return this.anggotaRepository.searchkodeAnggota(kodeAnggota);
	}

	public void update(AnggotaModel anggotaModel) {

		anggotaRepository.save(anggotaModel);
	}

	public void delete(AnggotaModel anggotaModel) {
		anggotaRepository.delete(anggotaModel);
	}

	public List<AnggotaModel> searchNamaAnggota(String namaAnggota) {
		return this.anggotaRepository.searchnamaAnggota(namaAnggota);

	}
}