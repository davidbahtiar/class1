package com.example.class1.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.class1.model.FakultasModel;
import com.example.class1.repository.FakultasRepository;

@Service
@Transactional
public class FakultasService {

	@Autowired
	private FakultasRepository fakultasRepository;
	
	

	public void save(FakultasModel fakultasModel) {
		fakultasRepository.save(fakultasModel);
		
	}
	
	public List<FakultasModel> read() {
		return this.fakultasRepository.findAll(); 
	}
	

	public FakultasModel searchKodeFakultas(String kodeFakultas) {
		
		return this.fakultasRepository.searchKodeFakultas(kodeFakultas);
	}

	public void update(FakultasModel fakultasModel) {
		
		fakultasRepository.save(fakultasModel);
	}

	public void delete(FakultasModel fakultasModel) {
		fakultasRepository.delete(fakultasModel);
	}
	
	public List<FakultasModel> searchNamaFakultas(String namaFakultas){
		return this.fakultasRepository.searchNamaFakultas(namaFakultas);
		
	}
	//pattern dari service 
	// public output namaMethod(tipe input1, tipe inputN){}
	public void create(FakultasModel fakultasModel) {
		
	}
	
	public void simpan(FakultasModel fakultasModel) {
		
	}
	
	public void insert(FakultasModel fakultasModel) {
		
	}
	public void mengubah(FakultasModel fakultasModel) {
		
	}
	
	public FakultasModel mencari(String namaFakultas) {
		return null;
	}
	public FakultasModel cariinDonk(String kodeFakultas, String namaFakultas) {
		return null;
		
	}
	public List<FakultasModel> cariKodeFakultasDonk(String kodeFakultas) {
		return this.fakultasRepository.cariFakultasDonkRepository(kodeFakultas);
		
	}
	
}

