package com.example.class1.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//BUAT MODEL DENGAN ATTRIBUT SEBAGAI BERIKUT :
// 1. TABEL : JURUSAN, KARYAWAN, KELAS, MAHASISWA, MATAKULIAH, NILAI, KOTA, BOBOTNILAI
// > JURUSAN
// 		-kodeJurusan(String), namaJurusan(String), kodeFakultas(String), kodeFakultas(String), gradeJurusan(Integer).

// > KARYAWAN
// 		-idKaryawan(Integer), kodeKaryawan(String), namaKaryawan(String), tglLahirKaryawan(Date), namaFoto(String), tipeFoto(String).

@Entity
@Table(name = "T_KARYAWAN")
public class KaryawanModel {

	@Id
	@Column(name = "ID_KARYAWAN")
	private Integer idKaryawan;

	public Integer getIdKaryawan() {
		return idKaryawan;
	}

	public void setIdKaryawan(Integer idKaryawan) {
		this.idKaryawan = idKaryawan;
	}

	public String getKdKaryawan() {
		return kdKaryawan;
	}

	public void setKdKaryawan(String kdKaryawan) {
		this.kdKaryawan = kdKaryawan;
	}

	public String getNmKaryawan() {
		return nmKaryawan;
	}

	public void setNmKaryawan(String nmKaryawan) {
		this.nmKaryawan = nmKaryawan;
	}

	public Date getTglLahirKaryawan() {
		return tglLahirKaryawan;
	}

	public void setTglLahirKaryawan(Date tglLahirKaryawan) {
		this.tglLahirKaryawan = tglLahirKaryawan;
	}

	public String getNmFoto() {
		return nmFoto;
	}

	public void setNmFoto(String nmFoto) {
		this.nmFoto = nmFoto;
	}

	public String getTpFoto() {
		return tpFoto;
	}

	public void setTpFoto(String tpFoto) {
		this.tpFoto = tpFoto;
	}

	@Column(name = "KD_KARYAWAN")
	private String kdKaryawan;

	@Column(name = "NM_KARYAWAN")
	private String nmKaryawan;

	@Column(name = "TGL_KARYAWAN")
	private Date tglLahirKaryawan;

	@Column(name = "NM_FOTO")
	private String nmFoto;

	@Column(name = "TP_FOTO")
	private String tpFoto;

	public Integer getidKaryawan() {
		return idKaryawan;
	}

	public void setidKaryawan(Integer idKaryawan) {
		this.idKaryawan = idKaryawan;
	}

	public String getkdKaryawan() {
		return kdKaryawan;
	}

	public void setkdKaryawan(String kdKaryawan) {
		this.kdKaryawan = kdKaryawan;
	}

	public String getnmKaryawan() {
		return nmKaryawan;
	}

	public void setnmKaryawan(String nmKaryawan) {
		this.nmKaryawan = kdKaryawan;
	}

	public Date gettglLahirKaryawan() {
		return tglLahirKaryawan;
	}

	public void tglLahirKaryawan(Date tglLahirKaryawan) {
		this.tglLahirKaryawan = tglLahirKaryawan;
	}

	public String getnmFoto() {
		return nmFoto;
	}

	public void setnmFoto(String nmFoto) {
		this.nmFoto = nmFoto;
	}

	public String gettpFoto() {
		return tpFoto;
	}

	public void settpFotoo(String tpFoto) {
		this.tpFoto = tpFoto;
	}

}
