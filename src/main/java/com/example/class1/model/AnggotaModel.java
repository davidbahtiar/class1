package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="T_ANGGOTA")

public class AnggotaModel {

	@Id
	@Column(name = "KD_ANGGOTA")
	private String kodeAnggota;
	
	@Column(name = "NM_ANGGOTA")
	private String namaAnggota;
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID_ANGGOTA" , nullable = false, length = 15 )
	private Integer idAnggota;
	
	@Column(name = "USIA")
	private Integer usiaAnggota;

	public void setKodeAnggota(String kodeAnggota) {
		// TODO Auto-generated method stub
		
	}

	public void setNamaAnggota(String namaAnggota) {
		// TODO Auto-generated method stub
		
	}

	public void setIdAnggota(Integer idAnggota) {
		// TODO Auto-generated method stub
		
	}

	public void setUsia(Integer usia) {
		// TODO Auto-generated method stub
		
	}
	
}
