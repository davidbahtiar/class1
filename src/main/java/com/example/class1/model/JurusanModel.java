package com.example.class1.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_JURUSAN")

public class JurusanModel {

	@Id
	@Column(name = "KD_JURUSAN")
	private String kdJurusan;
	private String nmJurusan;
	private String kdFakultas;
	private Integer gradeJurusan;

	public String getKdJurusan() {
		return kdJurusan;
	}

	public void setKdJurusan(String kdJurusan) {
		this.kdJurusan = kdJurusan;
	}

	public String getNmJurusan() {
		return nmJurusan;
	}

	public void setNmJurusan(String nmJurusan) {
		this.nmJurusan = nmJurusan;
	}

	public String getKdFakultas() {
		return kdFakultas;
	}

	public void setKdFakultas(String kdFakultas) {
		this.kdFakultas = kdFakultas;
	}

	public Integer getGradeJurusan() {
		return gradeJurusan;
	}

	public void setGradeJurusan(Integer gradeJurusan) {
		this.gradeJurusan = gradeJurusan;
	}

}
